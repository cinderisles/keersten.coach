
window.Parsley.addMessages('en', {
  type: {
    email:        "Not a valid email address",
  },
  required:       "Required",
});


var instance = $('#form').parsley();

instance.on('field:validated', function() {
  $('#notify').attr('disabled', !instance.isValid());
})
